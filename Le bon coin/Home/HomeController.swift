//
//  Project > Le bon coin
//  Filename > ViewController.swift
//
//  Created by Guillaume Gonzales on 04/10/2018.
//  Copyright © 2018 Tokidev S.A.S. - All rights reserved.
//

import UIKit
import Firebase

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let cellId = "cellId"
    var offers = [Offer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .gray
        
        collectionView.register(OfferCell.self, forCellWithReuseIdentifier: cellId)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        setupNavigationBarTitle()
        fetchOffers()
    }
    
    @objc func handleRefresh() {
        fetchOffers()
    }

    fileprivate func setupNavigationBarTitle() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysOriginal))
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.clipsToBounds = true
        navigationItem.titleView = titleImageView
    }
    
    fileprivate func fetchOffers() {
        offers.removeAll()
        
        let ref = Database.database().reference().child("offers")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            self.collectionView.refreshControl?.endRefreshing()
            
            guard let allOffers = snapshot.value as? [String: Any] else { return }
            
            allOffers.forEach({ (id, offerValue) in
                
                guard let offerDic = offerValue as? [String: Any] else { return }
                let offer = Offer(id: id, dictionary: offerDic)
                    
                self.offers.insert(offer, at: 0)
                self.offers.sort(by: { (p1, p2) -> Bool in
                    guard let d1 = Double(p1.price), let d2 = Double(p1.price) else { return p1.price.compare(p2.price) == .orderedDescending }
                    return d1 < d2
                })
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }) { (err) in
            print("Failed fetching offers:", err)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! OfferCell
        cell.offer = offers[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

