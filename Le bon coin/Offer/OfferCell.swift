//
//  OfferCell.swift
//  Le bon coin
//
//  Created by mbds on 05/10/2018.
//  Copyright © 2018 Tokidev S.A.S. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class OfferCell: UICollectionViewCell {
    
    var l = CALayer()
    
    var offer: Offer? {
        didSet {
            setOffer()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupCellUI()
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.orange
        return label
    }()
    
    let offerLabel: UILabel = {
        let label = UILabel()
        label.text = "Offer"
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.text = "Price"
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    fileprivate func setupCellUI() {
        
        addSubview(titleLabel)
        addSubview(offerLabel)
        addSubview(priceLabel)
        
        titleLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, paddingTop: 5, paddingLeft: 20, paddingBottom: 0, paddingRight: 8, width: 30, height: 20)
        
        offerLabel.anchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, paddingTop: 5, paddingLeft: 20, paddingBottom: 0, paddingRight: 8, width: 30, height: 20)
        
        priceLabel.anchor(top: offerLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, paddingTop: 5, paddingLeft: 20, paddingBottom: 0, paddingRight: 8, width: 30, height: 20)
    }
    
    fileprivate func setOffer() {
        guard let offer = offer else { return }
        
        offerLabel.text = offer.offer
        titleLabel.text = offer.title
        priceLabel.text = offer.price
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
