//
//  Project > Le bon coin
//  Filename > NewOfferController.swift
//
//  Created by Guillaume Gonzales on 04/10/2018.
//  Copyright © 2018 Tokidev S.A.S. - All rights reserved.
//

import UIKit
import Firebase

class NewOfferController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupNavigationBarTitle()
        setupTextFields()
     
    }
    
    let titleTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Title"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.borderStyle = .roundedRect
        textField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return textField
    }()
    
    let categoryTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Category"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.borderStyle = .roundedRect
        textField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return textField
    }()
    
    let cityTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "City"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.borderStyle = .roundedRect
        textField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return textField
    }()
    
    let offerTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Offer"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.borderStyle = .roundedRect
        textField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return textField
    }()
    
    let priceTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Price"
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.borderStyle = .roundedRect
        textField.addTarget(self, action: #selector(handleTextChange), for: .editingChanged)
        return textField
    }()
    
    let createButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Create", for: .normal)
        button.backgroundColor = UIColor(red: 255/255, green: 140/255, blue: 0/255, alpha: 0.5)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.layer.cornerRadius = 5
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleCreate), for: .touchUpInside)
        return button
    }()
    
    fileprivate func setupTextFields() {
        
        let stackView = UIStackView(arrangedSubviews: [titleTextField, categoryTextField, cityTextField, offerTextField, priceTextField, createButton])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        stackView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, paddingTop: 40, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, width: 0, height: 300)
    }
    
    @objc func handleTextChange() {
        let formValid = titleTextField.text?.isEmpty == false && categoryTextField.text?.isEmpty == false && offerTextField.text?.isEmpty == false && cityTextField.text?.isEmpty == false && priceTextField.text?.isEmpty == false
        
        if formValid {
            createButton.backgroundColor = UIColor(red: 255/255, green: 140/255, blue: 0/255, alpha: 1)
            createButton.isEnabled = true
        } else {
            createButton.backgroundColor = UIColor(red: 255/255, green: 140/255, blue: 0/255, alpha: 0.5)
            createButton.isEnabled = false
        }
    }
    
    @objc func handleCreate() {
        guard let title = titleTextField.text else { return }
        guard let category = categoryTextField.text else { return }
        guard let city = cityTextField.text else { return }
        guard let offer = offerTextField.text else { return }
        guard let price = priceTextField.text else { return }

        createButton.isEnabled = false

        let values = ["offer": offer, "title": title, "city": city, "category": category, "price": price] as [String: Any]
        
        let ref = Database.database().reference().child("offers").childByAutoId()
        ref.updateChildValues(values) { (err, ref) in
            if let err = err {
                self.createButton.isEnabled = true
                print("Failed adding new offer:", err)
                return
            }
            
            self.creationAlert()
        }
    }
    
    @objc func creationAlert() {
        let alertController = UIAlertController(title: "Le bon coin", message: "New offer added", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            self.dismiss(animated: true, completion: nil)
            self.clearTextFields()
        }
        alertController.addAction(okAction)

        present(alertController, animated: true, completion: nil)
        
        
    }
    
    fileprivate func setupNavigationBarTitle() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysOriginal))
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.clipsToBounds = true
        navigationItem.titleView = titleImageView
    }
    
    fileprivate func clearTextFields() {
        titleTextField.text = ""
        cityTextField.text = ""
        categoryTextField.text = ""
        offerTextField.text = ""
        priceTextField.text = ""
        
    }
    
}
