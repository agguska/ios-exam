//
//  Offer.swift
//  Le bon coin
//
//  Created by mbds on 05/10/2018.
//  Copyright © 2018 Tokidev S.A.S. All rights reserved.
//

import Foundation

struct Offer {
    var id: String
    var category: String
    var city: String
    var offer: String
    var price: String
    var title: String
    
    init(id: String, dictionary: [String: Any]) {
        self.id = id
        self.category = dictionary["category"] as? String ?? ""
        self.city = dictionary["city"] as? String ?? ""
        self.offer = dictionary["offer"] as? String ?? ""
        self.price = dictionary["price"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
    }

    
}
